var path = require('path');
var fs =require('fs');
var consts =require('../consts.js');

// Parse and activate modules
var moduleDir='./modules';
var distDir='./dist';

//main module
copyTextDir(path.join('./main','text'),'main');

//loop over modules
var moduleList=fs.readdirSync(moduleDir);
for (var moduleName of moduleList) {
	//Copy text files for different languages
	var textDir=path.join(moduleDir,moduleName,'text');
	copyTextDir(textDir,moduleName);
}


function copyTextDir(textDir,moduleName) {
	if (!fs.existsSync(textDir)) return;
	var langList=fs.readdirSync(textDir);
	for (var langName of langList) {
		console.log('Copy markdown files for module '+moduleName+' - language '+langName);
		fs.mkdirSync(path.join(distDir,'text',langName,moduleName),{recursive:true});
		var fileList=fs.readdirSync(path.join(textDir,langName));
		for (var fileName of fileList) {
			fs.copyFileSync(path.join(textDir,langName,fileName),path.join(distDir,'text',langName,moduleName,fileName));
		}
	}
}
