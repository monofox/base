#! /usr/bin/env node
var Templates = require('../main/server/templates/templates.js');

//Required libraries
var environment = process.env.NODE_ENV;
if (!environment) environment="production";
const serverConfig =require('../server.config.'+environment+'.json');

//Database connection
var mongoose = require('mongoose');

const main = async () => {
    mongoose.connect(serverConfig.MONGO_URL);
    mongoose.Promise = global.Promise;
    var db = mongoose.connection;
    mongoose.connection.on('error', console.error.bind(console, 'Error connection to MongoDB:'));
    await Templates.exportDefaultTemplates()
    db.close();
} 

main().then(() => process.exit())
