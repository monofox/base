'use strict'
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const TerserJSPlugin = require('terser-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const path = require('path')

module.exports = {
  entry: './app.js',
  stats: { children: false },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'index.js'
  },
  optimization: {
    minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})]
  },
  resolve: {
    alias: {
      Main: path.resolve(__dirname, 'main/')
    }
  },
  node: {
    Buffer: false,
    process: false
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options:
        {
          transformAssetUrls: {
            video: ['src', 'poster'],
            source: 'src',
            img: 'src',
            image: ['xlink:href', 'href'],
            use: ['xlink:href', 'href'],
            'tooltip-icon': 'src'
          }
        }
      },
      { resourceQuery: /blockType=i18n/, type: 'javascript/auto', loader: '@intlify/vue-i18n-loader' },
      { test: /\.css$/i, use: [MiniCssExtractPlugin.loader, 'css-loader'] },
      { test: /\.(mp3|ogg)$/i, loader: 'file-loader', options: { outputPath: 'audio/', esModule: false } },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          { loader: 'file-loader', options: { outputPath: 'img/', esModule: false } },
          { loader: 'image-webpack-loader', options: { bypassOnDebug: true, disable: true } }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{ loader: 'file-loader', options: { name: '[name].[ext]', outputPath: 'fonts/' } }]
      },
      { test: /\.md$/i, use: 'raw-loader' }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new CopyWebpackPlugin([
      { from: './html/index.html' },
      { from: './html/favicon.ico' },
      { from: './html/apple-touch-icon.png' },
      { from: './html/android-icon.png' },
      { from: './html/manifest.json' }
    ])
  ]
}
