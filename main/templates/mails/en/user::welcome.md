% Title: Welcome to {{config.SITE_NAME}}

Dear {{user.name}},

welcome to {{config.SITE_NAME}}. Your account is ready.

Go to [{{instanceUrl}}]({{instanceUrl}}) and start using our tools.

Your Lerntools-Team