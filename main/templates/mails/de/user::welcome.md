% Title: Willkommen zu {{config.SITE_NAME}}

Hallo {{user.name}},

Willkommen zu {{config.SITE_NAME}}. Dein Konto ist jetzt bereit.

Geh' auf [{{instanceUrl}}]({{instanceUrl}}) und leg' los!

Dein Lerntools-Team