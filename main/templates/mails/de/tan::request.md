% Title: Transaktionscode

Hallo {{user.name}},

der Code zur Bestätigung Deiner Aktion lautet: {{tan}}.

Solltest Du den Code nicht angefordert haben, ignoriere diese E-Mail bitte.
