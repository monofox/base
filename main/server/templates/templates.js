/**
 * File to handle templates (e.g. markdown)
 */

var md = require('markdown-it')();
const MarkdownItAttrs = require('markdown-it-attrs');
const MarkdownItTexmath = require('markdown-it-texmath');
const MarkdownItUnderline = require('markdown-it-underline');
const MarkdownItBlockquoteCite = require('markdown-it-blockquote-cite').default;
md.use(MarkdownItAttrs);
md.use(MarkdownItTexmath);
md.use(MarkdownItUnderline);
md.use(MarkdownItBlockquoteCite);

var mustache = require('mustache');
const htmlToText = require('html-to-text');

const debug=require('debug')('templates');
var Template = require('./model');
const consts=require ('../../../consts.js');
const path=require('path');
var fs = require('fs');
const MAIN_MODULE = 'main';

const TEMPLATE_TYPES=[];

function getTypeInformation(tplType) {
    var t = tplType.split('::');
    if (t.length < 3) return;

    return {
        category: t[0],
        module: t[1],
        type: t.slice(2).join('::')
    }
}

function addTemplateType(tplType) {
    if (TEMPLATE_TYPES.indexOf(tplType) < 0 && tplType.split('::').length > 2) {
        TEMPLATE_TYPES.push(tplType);
    }
}

function getPathToTemplate(tplType, lang) {
    var tplInfo = (typeof tplType === 'object') ? tplType : getTypeInformation(tplType);
    if (!tplInfo) return null;

    var basePath = path.join(__dirname, '..', '..');
    if (tplInfo.module != MAIN_MODULE) {
        basePath = path.join(basePath, '..', 'modules', tplInfo.module);
    }
    basePath = path.join(basePath, 'templates', tplInfo.category, lang, tplInfo.type + '.md');
    return basePath;
}

exports.getValidTypes = function() {
    return TEMPLATE_TYPES;
}

exports.isValidType=function(tplId) {
    return TEMPLATE_TYPES.indexOf(tplId) > -1;
}

exports.getTypeInformation = getTypeInformation;
exports.addTemplateType = addTemplateType;
exports.registerTemplateTypes=function(tplTypeList) {
    tplTypeList.forEach((tplType) => addTemplateType(tplType));
}
exports.registerModuleTemplate = function(moduleId, templateType) {
    try {
        var tplId = templateType.split('::')[0] + '::' + moduleId + '::' + templateType.split('::').slice(1).join('::');
        debug('Register module template: ', tplId);
        if (tplId) {
            addTemplateType(tplId);
        }
    } catch(error){
        debug('Failed registering module template: ', error);
    };
}

exports.loadDefault=async function() {
    debug('Find default templates.', TEMPLATE_TYPES);
    TEMPLATE_TYPES.forEach((element) => {
        var tplInfo = getTypeInformation(element);
        consts.SUPPORTED_LANGUAGES.forEach((lang) => {
            var mdPath = getPathToTemplate(tplInfo, lang);
            fs.stat(mdPath, (error, stats) => {
                if (error === null) {
                    var tplFile = {
                        'tplId': element,
                        'language': lang,
                        'path': mdPath,
                        'stat': stats
                    };
                    debug("Template file found: ", mdPath);
                    Template.findOne({type: tplFile.tplId, language: tplFile.language, defaultTpl: true}, (err, tplObject) => {
                        if (err || !tplObject || tplObject.updatedAt < stats.mtime) {
                            debug('Update of file %s required.', mdPath)
                            fs.readFile(tplFile.path, 'utf8', function (err,data) {
                                if (!err) {
                                    var posPageTitle = data.search(/% Title: (.*)\n/);
                                    var newLine = data.substring(posPageTitle).search(/\n/);
                                    var title = data.substring(posPageTitle+9, newLine);
                                    data = data.substring(posPageTitle+newLine+2);
                                    if (!tplObject) {
                                        tplObject = new Template(
                                            {
                                                type: tplFile.tplId,
                                                language: tplFile.language,
                                                title: title,
                                                content: data,
                                                defaultTpl: true
                                            }
                                        );
                                    } else {
                                        tplObject.title = title;
                                        tplObject.content = data;
                                    }
                                    tplObject.save().catch((error) => {
                                        debug("Template %s could not be written: ", mdPath, error);
                                    });
                                } else {
                                    debug("Template file %s could not be read: ", mdPath);
                                }
                            });
                        }
                    });
                } else {
                    debug("Template file not found: ", mdPath);
                }
            });
        });
    });
}

function saveTemplate(tpl) {
    var mdPath = getPathToTemplate(tpl.type, tpl.language);
    var content = '% Title: ' + tpl.title + "\n\n" + tpl.content;
    try {
        fs.writeFileSync(mdPath, content)
        console.log('File %s was written', mdPath);
    } catch (err) {
        if (err) {
            console.error('File %s could not be written: %s', mdPath, err);
        }
    };
}

exports.exportDefaultTemplate=saveTemplate;

exports.exportDefaultTemplates=async function(callback = undefined) {
    return Template.find({defaultTpl: true}, (err, tplObject) => {
        tplObject.forEach((tpl) => {saveTemplate(tpl);});
    })
}


exports.getTemplate=async function(tplId, language) {
    debug('Request to fetch template ' + tplId + ' in language ' + language);
    // Check if available in database.
    var tpl = await Template.findOne({ type: tplId, language: language }, (err, tplObject) => {
        if (err) {
            debug("Error searching tpl " + tplId + " for language " + language + " => " + err);
        }
    });

    if (!tpl) {
        throw "Template not found: " + tplId + " in language " + language;
    }

    return tpl;
}

function parseTemplate(tpl, options) {
    var ret = {
        title: "",
        content: {
            html: "",
            text: ""
        }
    }
    var preparedData = mustache.render(tpl.content, options);
    ret.title = mustache.render(tpl.title, options);
    ret.content.html = md.render(preparedData, {});
    ret.content.text = htmlToText.convert(ret.content.html);

    return ret;
}

exports.parseTemplate = parseTemplate;

exports.parseReplaceTemplate = function(tpl, options) {
    var data = parseTemplate(tpl, options);
    tpl = tpl.toObject();
    tpl.title = data.title;
    tpl.content = {
        html: data.content.html,
        text: data.content.text,
        md: tpl.content
    }
    return tpl;
}

exports.getById = async function(tplId) {
    return await Template.findOne({_id: tplId});
}
