const debug = require('debug')('templates');
//Internal libs
const consts = require('../../../consts.js');
const config = __config;

//Database schemas
var Template = require('./model');
var Templates = require('./templates');

/**
 * Get a list of templates
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getList = function (req, res, next) {
    var filter = {};
    var lang = req.query.lang || null;
    var defaultTpl = req.query.include_default || false;
    if (lang) {
        filter.language = lang;
    }
    if (!defaultTpl) {
        filter.defaultTpl = false;
    }
    if (req.query.only_default) {
        filter.defaultTpl = true;
    }
    if (req.query.type) {
        filter.type = req.query.type;
    }
    Template.find(filter).sort({ type: 'asc', language: 'asc', defaultTpl: 'asc' }).exec(function (err, tplList) {
        if (err) return res.status(500).json([{ msg: 'database-error' }]);
        return res.status(200).json(tplList);
    });
}

exports.getListAvailable = function(req, res, next) {
    return res.status(200).json(Templates.getValidTypes());
}

exports.get = async function (req, res, next) {
    var tpl = await Templates.getById(req.params.id);
    if (tpl) {
        return res.status(200).json(tpl);
    } else {
        return res.status(404).end();
    }
}

exports.getByTypeLang = async function (req, res, next) {
    var tplId = req.params.tplId;
    var lang = req.params.lang;
    var firstOnly = !req.query.firstOnly || req.query.firstOnly === 'true';
    var convert = req.query.convert === 'true';
    var filter = {
        type: tplId,
        language: lang
    };
    var replaceOption = {};

    // sort defaultTpl 1 => false first, -1 => true first
    Template.find(filter).sort({ type: 'asc', language: 'asc', defaultTpl: 1 }).exec(function (err, tplList) {
        if (err) return res.status(500).json([{ msg: 'database-error' }]);
        if (tplList.length <= 0) return res.status(404).end();
        var tplData = firstOnly ? tplList[0] : tplList;
        if (convert && firstOnly) {
            tplData = Templates.parseReplaceTemplate(tplData, replaceOption);
        } else if (convert) {
            tplData = tplList.map((tpl) => {
                return Templates.parseReplaceTemplate(tpl, replaceOption);
            });
        }

        return res.status(200).json(tplData);
    });
}

exports.create = async function (req, res, next) {
    var reqTpl = new Template(req.body);
    var errorList = await validateTemplate(reqTpl);

    // Stopp here if there is an error.
    if (errorList.length > 0) {
        return res.status(400).json(errorList);
    }

    reqTpl.save(async function(err) {
        if (err) {debug(err); return res.status(500).json([{msg:'unknown-error'}]);}
        // Get object.
        var tpl = await Template.findOne({type: reqTpl.type, language: reqTpl.language});
        if (!tpl) {debug(err); return res.status(500).json([{msg:'unknown-error'}]);}

        return res.status(201).json(tpl);
    });
}

exports.update = async function (req, res, next) {
    var reqTpl = new Template(req.body);
    var errorList = await validateTemplate(reqTpl, req.params.id);

    // Check if the tpl exists.
    var tpl = await Templates.getById(req.params.id);
    if (!tpl) {
        return res.status(404).end();
    }

    // Stopp here if there is an error.
    if (errorList.length > 0) {
        return res.status(400).json(errorList);
    }

    tpl.type = reqTpl.type;
    tpl.language = reqTpl.language;
    tpl.title = reqTpl.title;
    tpl.content = reqTpl.content;

    tpl.save(async function(err) {
        if (err) {debug(err); return res.status(500).json([{msg:'unknown-error'}]);}
        // Get object.
        var tpl = await Templates.getById(req.params.id);
        if (!tpl) {debug(err); return res.status(500).json([{msg:'unknown-error'}]);}
        // If it is a standard template, we want to export it to local file storage.
        if (tpl.defaultTpl) {
            Templates.exportDefaultTemplate(tpl);
        }
        return res.status(200).json(tpl);
    });
}

exports.delete = async function (req, res, next) {
    var tplId = req.params.id;
    var tpl = await Template.findOne({_id: tplId});
    if (tpl && tpl.defaultTpl) {
        return res.status(409).json([
            {
                msg: "default-tpl-not-deletable"
            }
        ]);
    } else if (tpl) {
        tpl.delete();
        return res.status(204).end();
    } else {
        debug("Template not found: " + tplId);
        return res.status(404).end();
    }
}

async function validateTemplate(tpl, id = null) {
    var errorList = [];
    if (!Templates.isValidType(tpl.type)) {
        errorList.push({
            field: 'type',
            msg: 'invalid-tpl-type'
        });
    }
    if (consts.SUPPORTED_LANGUAGES.indexOf(tpl.language) == -1) {
        errorList.push({
            field: 'language',
            msg: 'lang-not-supported'
        });
    }
    if (!tpl.title && tpl.title < 1) {
        errorList.push({
            field: 'title',
            msg: 'title-required'
        });
    }
    if (!tpl.content && tpl.content < 1) {
        errorList.push({
            field: 'content',
            msg: 'content-required'
        });
    }

    // Ensure, that the changes do not conflict.
    if (errorList.length <= 0) {
        var filter = {
            $and: [
                {type: tpl.type, language: tpl.language, defaultTpl: tpl.defaultTpl}
            ]
        };
        if (id) {
            filter.$and.push({_id: {$ne : id}});
        }
        var dupTpl = await Template.findOne(filter);
        if (dupTpl) {
            errorList.push({
                field: 'body',
                msg: 'tpl-already-exist'
            });
        }
    }

    return errorList;
}
