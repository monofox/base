var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var RefreshTokenSchema=new Schema({
	login:		{type: String, required: true},
	expires:	{type: Date, required: true},
	ip:			{type: String, required: true},
	token:		{type: String, required: true}
});


module.exports=mongoose.model('RefreshToken', RefreshTokenSchema );
