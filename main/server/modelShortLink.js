var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var ShortLinkSchema=new Schema({
	url :		{type: String, required: true, max: 1000},
	ticket :	{type: String, required: true, max: 100},
	ts:			{type: Number, required: true, default: 0},
	count:		{type: Number, default:0 }
});

module.exports=mongoose.model('ShortLink', ShortLinkSchema );
