import MarkdownIt from 'markdown-it';
import MarkdownItAttrs from 'markdown-it-attrs';
import MarkdownItTexmath from 'markdown-it-texmath';
import MarkdownItUnderline from 'markdown-it-underline';
import MarkdownItBlockquoteCite from 'markdown-it-blockquote-cite';

//init markdown
var markdownIt=new MarkdownIt({breaks:true});
markdownIt.use(MarkdownItAttrs);
markdownIt.use(MarkdownItTexmath);
markdownIt.use(MarkdownItUnderline);
markdownIt.use(MarkdownItBlockquoteCite);

export default {
	/*
	convert markdown string to html using inline mode → result is not wrapped into an <p> paragraph
	line breaks are converted to <br>
	*/
	render: function(md) {
		var html=markdownIt.renderInline(md);
		//return html.substring(3,html.length-6)
		return html;
	},
	renderFull: function(md) {
		var html=markdownIt.render(md);
		return html;
	}
}
