import axios from 'axios';
import config from '../../app.config.js';

/* Currently not used - for future use*/
export default {
	//Donwload Media and return as base64 encoded string
	getMediaBase64Url: async function(url) {
		try {
			var response=await axios.get(url,{ responseType: 'arraybuffer'});
			var base64url='data:'+response.headers['content-type']+';base64,'+Buffer.from(response.data, 'binary').toString('base64');
			return base64url;
		}
		catch (err) {
			console.log(err);
			return '';
		}
	}
}
