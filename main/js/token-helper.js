import store from './store.js';

export default {
	// try to refresh access token
	refreshAccessToken: async function (axios) {
		try {
			const result=await axios.post('main/token', { login:store.user.login, token:store.user.refreshToken });
			//exit if there is no token in response
			if (!result.data.accessToken) return null;
			//refresh successfull
			var accessToken=result.data.accessToken;
			console.debug("Succesfull Token Refresh");
			return accessToken;
		} catch (err) {
			console.debug(err);
			return null;
		}
	}
}
