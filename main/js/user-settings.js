import store from './store.js'

export default {
  updateLanguage: function (ref, lang) {
    // TODO: Perhaps worth to check against a list of known lang values?
    if (lang) {
      store.lang = lang
      ref.$i18n.locale = lang
      ref.$root.$i18n.locale = lang
      document.documentElement.setAttribute('lang', lang)
      console.debug('Changed language to ' + lang)
    } else {
      console.warn('Invalid language requested: ' + lang)
    }
  },
  updateUserProfile: function (ref, userData) {
    store.user.name = userData.name
    store.user.login = userData.login
    store.user.email = userData.email
    this.updateLanguage(ref, userData.lang)
  }
}
