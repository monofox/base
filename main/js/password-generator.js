const CHARS="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

export default {
	/*
	Genrate a random string for passwords or tickets
		options.length: number of characters
		options.numbers: flag if numbers should be used (aside to small and capital letters)
	*/
	generate:function(options) {
		//set default options, if options or length missing
		if (!options || !options.length) options={ length:20, numbers:true};
		//generate random string, using normal Math.rand function (its no banking app...)
		var result='';
		for (var i=0; i<=options.length; i++) {
			var index;
			if (options.numbers) index=Math.floor(Math.random()*CHARS.length);
			else index=Math.floor(Math.random()*(CHARS.length-10));
			result+=CHARS.charAt(index);
		}
		return result;
	}
}
