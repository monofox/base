var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
var path = require('path');
var fs =require('fs');

//Debugging
var debug = require('debug')('app');

// Read configuration and constants
var environment = process.env.NODE_ENV;
var port= process.env.PORT;
if (!environment) environment="production";
if (!port) port=8082;
var config =require('./server.config.'+environment+'.json');

//CORS - Domains whitelisted in server.config.json
var cors=require('cors');
var corsOptions = {
	origin: function (origin, callback) {
    	if (origin === undefined || config.CORS_SITES.indexOf(origin) !== -1) { callback(null, true) }
		else { callback(new Error('Not allowed by CORS: '+origin)) }
	}
}

//Protect application
var helmet = require('helmet');
app.use(helmet());

//global Variables
global.__basedir = __dirname; 	//Basedir of application
global.__modules = [];			//List of all modules
global.__config = config;		//Configuration settings
global.__namespaces=[];			//List of all namespaces
// Database setup
var mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.connect(config.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//Authorization
var mainController=require('./main/server/controller.js');
app.use(mainController.parseAuthorizationHeader);

//Other settings
var bodyParser = require('body-parser')
app.use(bodyParser.json({limit: '50mb'}))
app.use(bodyParser.urlencoded({ extended: false }));

// static routes - assets and external libs
app.get('/robots.txt', function (req, res) { res.sendFile(path.join(__dirname,'html/robots.txt'));});
app.get('/apple-touch-icon.png', (req,res) => {res.sendFile(path.join(__dirname, "dist/apple-touch-icon.png"))});
app.get('/favicon.ico', (req,res) => {res.sendFile(path.join(__dirname, "dist/favicon.ico"))});

// upload dir
if (config.UPLOAD_DIR) app.use('/upload', express.static(config.UPLOAD_DIR));

//redirect to app main page
app.get('/', function (req, res) { res.redirect('app'); });

//Activate main Server
var modulePath=path.join(config.API_URL,'main');
var routerFile=path.join('main','server','router.js');
if (config.CORS_SITES && config.CORS_SITES.length>0) app.use(modulePath, cors(corsOptions));
app.use(modulePath, require('./'+routerFile));

// Parse and activate modules
const baseServerModule = require('./server/init');
const baseServer = new baseServerModule.BaseServer();
var modulePaths = [path.resolve('./main')];
var moduleDir=config.MODULE_DIR;
if (fs.existsSync(moduleDir)) {
	var folderList=fs.readdirSync(moduleDir);
	for (var folder of folderList) {
		//Add to namespaces
		global.__namespaces.push(folder);
		//Server side routing for api
		var moduleApiPath=path.join(config.API_URL,folder);
		var modulePath = path.resolve(path.join(moduleDir, folder));
		modulePaths.push(modulePath);
		var routerFile=path.join(moduleDir,folder,'server','router.js');
		if (fs.existsSync(routerFile)) {
			debug("...activating server side api for "+folder);
			if (config.CORS_SITES && config.CORS_SITES.length>0) app.use(moduleApiPath, cors(corsOptions));
			app.use(moduleApiPath, require('./'+routerFile));
			global.__modules.push(folder);
		}
	}
}
baseServer.registerModuleList(modulePaths).then(() => {
	baseServer.postLoadModules();
});

//Serve client app
app.use('/app', express.static('dist/'))

// Minimal logging
var logger = require('morgan');
app.use(logger('tiny'));

// Catch 404 error
var createError = require('http-errors');
app.use(function(req, res, next) {  next(createError(404)); });

// Handle errors
app.use(function(err, req, res, next) {
	debug(err);
	res.status(err.status || 500);
	if (err.status==404) res.sendFile(path.join(__dirname, "html/custom_404.html"))
	else  res.sendFile(path.join(__dirname, "html/custom_502.html"))
});

// Start listening
app.listen(port)
