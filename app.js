/**********************************************************
 * Initialize App - don't change anything from here!
 **********************************************************/
import Vue from 'vue';
import App from './App.vue';

//Load config
import AppConfig from './app.config.js';
Vue.prototype.$config=AppConfig;

//Import constants
import Consts from './consts.js';
Vue.prototype.$consts = Consts;

//Configure vue router
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import modMain from 'Main/config';
var routes=modMain.routes;
for (var mod of AppConfig.allModules) {
	if (mod.routes) routes=routes.concat(mod.routes)
}
routes.push({ path: '/:ticket', name:'main-notfound', component: () => import('Main/views/ShortLink.vue')}) //default route
const router = new VueRouter({ routes:routes })

//Prepare calls to backend
import axios from 'axios';
import store from 'Main/js/store.js';
import tokenHelper from 'Main/js/token-helper.js';
axios.defaults.baseURL=AppConfig.urlBackend;
Vue.prototype.$axios = axios;

//Intercept request and insert Authorization (accessToken)
axios.interceptors.request.use(
	async config => {
		var accessToken = store.user.accessToken;
		config.headers.Authorization='Bearer '+accessToken;
		return config;
	},
	error => {
		Promise.reject(error)
	}
);

//Intercept answers and check for error codes
axios.interceptors.response.use((response) => { return response }, async function (error) {
	const originalRequest = error.config;
	//handle error not-authorized
	if (error.response.status === 401 && !originalRequest._retry) {
		//remove invalid accessToken
		store.user.accessToken="";
		//avoid loop on 401 errors
		originalRequest._retry = true;
		//try to refresh token
		var accessToken = await tokenHelper.refreshAccessToken(axios);
		//failed to refresh token
		if (!accessToken) {
			store.user.clear();
			var queryParms = {};
			if (router.currentRoute.path !== undefined && router.currentRoute.path !== null) {
				queryParms['redirect'] = router.currentRoute.path;
			}
			return router.push({name:'main-login', query: queryParms});
		}
		//successfully refreshed token
		store.user.accessToken=accessToken;
		originalRequest.Authorization='Bearer ' + accessToken;
		//resend original request with new token
		return axios(originalRequest);
	}
	//handle other errors
	if (error.response.status==403) router.push({name:'main-forbidden'});
	if (error.response.status==500) router.push({name:'main-error'});
	return Promise.reject(error);
});

// Create VueI18n instance with options
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);
const i18n = new VueI18n({
  locale: AppConfig.defaultLocale,
  silentFallbackWarn: true
})

//Validation
import validator from "Main/js/validator.js";
Vue.prototype.$v=validator.validate;

//Start app
var app = new Vue({
	el: '#app',
	render: h => h(App),
	router:router,
	i18n:i18n
})
