<a href="https://weblate.bubu1.eu/engage/lerntools/">
<img src="https://weblate.bubu1.eu/widgets/lerntools/-/main/svg-badge.svg" alt="Translation Status" />
</a>

# LernTools - Tools for digital teaching with a focus on privacy

There are many good tools on the internet that support collaborative learning - but unfortunately they often use trackers or involve resources from third parties. This situation was the starting point for developing simple tools that do not process any personal data of students and do not track them or their actions.

![Main menu](https://gitlab.com/lerntools/base/-/raw/master/doc/examples/index.png)

## Components of the application

The application is strictly modularized, where the following modules are available in separate repositories:

- **abcd**: Interactive quiz, technically spoken an simple audio response system (a bit like kahoot)
- **ideas**: Collaborative online brainstorming for groups of students (a bit like padlet)
- **survey**: Survey tool - create simple surveys with three question types (yes / no, agreement, free text)
- **projector**: Online projector - allow students to transfer photos to the lecturer's browser.
- **tex**: Tex Trainer - exercise the TeX syntax for math formulas
- **about**: About Information - add personalized information to your instance

Students in generall do not have to register, all apps can be accessed vi qr-code or link + ticket.


## Technical basics

The learning tools are a "full-stack Javascript" application. This means that only Javascript is used on both the server and the client.
Unfortunately, that means that the application won't run in the classic LAMP stack - you will need a MEVN stack (MongoDB - Express - Vue - Nodejs) instead.
The advantage is that as a developer you only have to master one programming language and modern web technologies (such as web sockets) can be used directly and easily.

The application bases on the following components:

- **Nodejs**: Runtime environment for server-side Javascript (see also [https://nodejs.org/en/](https://nodejs.org/en/))
- **Express**: Server-side framework for developing web applications using Javascript (see also [https://expressjs.com/](https://expressjs.com/)).
- **Vue**: Cient-side JS-framework, similar to Angular or React (currently Version 2)
- **MongoDB**: Non-relational database, which can be used to save documents at a piece (without paying attention to the classical normal forms ...).
This simplifies the development significantly, since objects can be stored directly. The framework Mongoose is used to connect nodejs and monbgodb.
- **Bootstrap**: Extensive CSS (and Javascript) framework, which allows easy design of ergonomic and responsive surfaces.
- **katex**: Representing mathematical formulas (TeX syntax) in the browser (via Javascript)

## Documentation

See [Wiki](https://codeberg.org/lerntools/base/wiki/Index)

## License

[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.de.html)

### Dependencies

As noted in `Technical basics`, Lerntools use several different external libraries and components. These libraries are released on own licenses which we want partly to explicitly attribute here:

 - [vue-material-design-icons](https://www.npmjs.com/package/vue-material-design-icons): MIT license
 - [material-icons](https://www.npmjs.com/package/material-icons): Apache-2.0

## Disclaimer of Warranty (see GNU General Public License v3.0)

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.

## Limitation of Liability (see GNU General Public License v3.0)

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
